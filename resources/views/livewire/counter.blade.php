<div style="text-align: center" x-data="{ open: false }">
    <button wire:click="increment">+</button>
    <h1>{{ $count }}</h1>
    <div>
        <button @click="open = true">Show More...</button>
        <ul x-show="open" @click.away="open = false">
            <li><button wire:click="resetCount">reset</button></li>
            <li><button @click="alert('hoge')">hoge</button></li>
        </ul>
    </div>
</div>
